How to Build & Run:
-------------------
+ ANT is the build tool. 
+ Build configuration in the build.xml 
+ Three important ANT targets are
	- Test: 
		- Runs all the test cases for the application.
		- CMD: ant test
	- Package:
		- Packages the application, creates the JAR
		- CMD: ant package
	- Run:
		- Runs the above packaged application 
		- CMD: ant run -DfileName=/path/to/file -Dmode=(modeA|modeB)
		- The mode values case insensitive


Assumption:
-----------
+ The input data file size is not a concern, will not crash the application. The input data is read into memory all at once.   
+ This is a command line application and does not need a shared concurrent queue where requests are placed and then are consumed by another elevator process.  
+ All the commands received in a line of input data are received consecutively not concurrently.
+ Deriving requirements from output of Line 5, indicate that the elevator choose a direction(Up/Down) based on the first input request.


Design Flow:
------------
The application's flow is
	1. Read & Validate Input
	2. Process Input
	   + The Application's Modes are encapsulated in Strategy Classes, which are chosen at application startup; The Strategy Pattern has been used.

Vocabulary:
-----------
	InputData: Class encapsulates the entire input data.
	FloorRun:  Class represents a line in the input file- contains the start floor and all the floor requests.   
	Request:   Class represents an elevator command to move from one floor to another. Has a start & end floor. 
	Response:  Class encapsulates the response from the elevatorService.