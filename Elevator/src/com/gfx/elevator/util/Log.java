package com.gfx.elevator.util;


/**
 * Class for Logging, can be later replaced by a logger framework.
 * 
 * @author tmokha
 *
 */
public class Log
{
	public static void log(String msg)
	{
		System.out.println(msg);
	}

	public static void error(String msg)
	{
		System.err.println(msg);
	}

	public static void fatal(String msg)
	{
		System.err.println(msg);
	}
}