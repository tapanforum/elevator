package com.gfx.elevator.model;

import com.google.common.base.Objects;

public class Request
{
	private int startFloor;
	private int endFloor;
	private Direction direction;

	public Request(int startFloor, int endFloor)
	{
		this.startFloor = startFloor;
		this.endFloor = endFloor;

		//Derive direction from floors
		this.direction = Direction.get(startFloor, endFloor);
	}

	public int getStartFloor()
	{
		return this.startFloor;
	}

	public int getEndFloor()
	{
		return this.endFloor;
	}

	public Direction getDirection()
	{
		return this.direction;
	}

	public int hashCode()
	{
		return Objects.hashCode(new Object[]
		{ Integer.valueOf(this.startFloor), Integer.valueOf(this.endFloor) });
	}

	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		return (Objects.equal(Integer.valueOf(this.startFloor), Integer.valueOf(other.startFloor)))
				&& (Objects.equal(Integer.valueOf(this.endFloor), Integer.valueOf(other.endFloor)));
	}

	public String toString()
	{
		return "StartFloor:" + this.startFloor + ", EndFloor:" + this.endFloor;
	}
}