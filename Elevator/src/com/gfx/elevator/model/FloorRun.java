package com.gfx.elevator.model;

import java.util.Queue;

public class FloorRun
{
	private int startFloor;
	private Queue<Request> queue;

	public FloorRun(int startFloor, Queue<Request> queue)
	{
		this.startFloor = startFloor;
		this.queue = queue;
	}

	public int getStartFloor()
	{
		return this.startFloor;
	}

	public Queue<Request> getQueue()
	{
		return this.queue;
	}

	public String toString()
	{
		return "StartFloor:" + this.startFloor + ", Queue:" + this.queue;
	}
}