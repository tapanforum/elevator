package com.gfx.elevator.model;

public enum Mode
{
	ModeA, ModeB;

	public static Mode getMode(String modeName)
	{
		if ("ModeA".equalsIgnoreCase(modeName))
			return ModeA;
		if ("ModeB".equalsIgnoreCase(modeName))
		{
			return ModeB;
		}
		return null;
	}
}