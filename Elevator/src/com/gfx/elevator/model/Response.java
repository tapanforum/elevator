package com.gfx.elevator.model;

import com.google.common.base.Joiner;
import java.util.Deque;
import java.util.Queue;

public class Response
{
	private int count;
	private Deque<Integer> queue;

	public Response(Deque<Integer> responseQueue, int count)
	{
		this.queue = responseQueue;
		this.count = count;
	}

	public int getCount()
	{
		return this.count;
	}

	public Queue<Integer> getQueue()
	{
		return this.queue;
	}

	public String getDisplayString()
	{
		return Joiner.on(" ").join(this.queue) + " (" + this.count + ")";
	}
}