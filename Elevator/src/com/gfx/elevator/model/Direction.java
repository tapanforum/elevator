package com.gfx.elevator.model;

public enum Direction
{
	UP, DOWN;

	public static Direction get(int startFloor, int endFloor)
	{
		if (startFloor < endFloor)
		{
			return UP;
		}
		else
		{
			return DOWN;
		}
	}
}