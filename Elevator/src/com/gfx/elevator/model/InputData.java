package com.gfx.elevator.model;

import java.util.List;

public class InputData
{
	private Mode mode;
	private List<FloorRun> floorRuns;

	public InputData(List<FloorRun> floorRuns, Mode mode)
	{
		this.mode = mode;
		this.floorRuns = floorRuns;
	}

	public Mode getMode()
	{
		return this.mode;
	}

	public List<FloorRun> getFloorRuns()
	{
		return this.floorRuns;
	}

	public String toString()
	{
		return "Mode:" + this.mode + ", FloorRuns:" + getFloorRuns();
	}
}