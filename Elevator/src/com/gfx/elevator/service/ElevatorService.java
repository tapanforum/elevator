package com.gfx.elevator.service;

import com.gfx.elevator.model.FloorRun;
import com.gfx.elevator.model.Mode;
import com.gfx.elevator.model.Response;
import com.gfx.elevator.service.algo.MoveStrategy;

/**
 * Elevator Service takes in a floorRun and responds with a moves.   
 * 
 * @author tmokha
 */
public class ElevatorService
{
	private FloorRun floorRun;
	MoveStrategy moveStrategy;

	public ElevatorService(FloorRun floorRun, Mode mode)
	{
		this.floorRun = floorRun;

		this.moveStrategy = MoveStrategy.instance(mode);
	}

	/**
	 * Executes strategy based on input, most of the logic is in the move strategy.  
	 * 
	 * @return
	 */
	public Response execute()
	{
		return this.moveStrategy.execute(this.floorRun.getStartFloor(), this.floorRun.getQueue());
	}
}