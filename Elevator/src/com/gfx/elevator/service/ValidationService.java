package com.gfx.elevator.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.gfx.elevator.model.FloorRun;
import com.gfx.elevator.model.InputData;
import com.gfx.elevator.model.Mode;
import com.gfx.elevator.model.Request;
import com.gfx.elevator.util.Log;

/**
 * Validates Input
 * 
 * @author tmokha
 */
public class ValidationService
{
	/**
	 * Validates Input
	 * 
	 * @param args
	 * @return
	 */
	public InputData validate(String[] args)
	{
		// Validate Arguments
		ImmutablePair<List<String>, Mode> validatedArguments = validateArguments(args);

		// Validate Data
		InputData inputData = validateData(validatedArguments);

		// Return
		return inputData;
	}

	/**
	 * Validate arguments
	 * 
	 * @param args
	 * @return
	 * @throws IllegalArgumentException - Do not handle, let system fail, fatal error.
	 */
	protected ImmutablePair<List<String>, Mode> validateArguments(String[] args)
	{
		try
		{
			// Validate if filename and mode are supplied
			if (args.length != 2)
			{
				throw new IllegalArgumentException("Please supply two arguments");
			}

			// Validate if correct Mode is Supplied
			Mode mode = null;
			if ((mode = Mode.getMode(args[1])) == null)
			{
				throw new IllegalArgumentException("Unknown mode name: " + args[1]);
			}

			// Validate if readable file is provided
			List<String> lines = null;
			try
			{
				lines = Files.readAllLines(Paths.get(args[0], new String[0]));
			}
			catch (IOException e)
			{
				throw new IllegalArgumentException("Cannot load File: " + args[0]);
			}

			// Return - both arguments are valid
			return ImmutablePair.of(lines, mode);
		}
		catch (IllegalArgumentException e)
		{
			// Display generic usage followed by argument specific message
			Log.fatal("Usage: ant run fileName modeName");
			Log.fatal(e.getMessage());

			// Re-throw, should not be handled, fatal error
			throw e;
		}
	}

	/**
	 * Validate Data
	 * 
	 * @param validatedArguments
	 * @return
	 */
	protected InputData validateData(ImmutablePair<List<String>, Mode> validatedArguments)
	{
		//Store FloorRuns
		List<FloorRun> processedFloorRuns = new ArrayList<>();
		
		//Foreach InputLine convert to FloorRun
		for (String floorRun : validatedArguments.getLeft())
		{
			//Validate Line
			if (!isValidLine(floorRun))
			{
				//Log Invalid Line
				Log.error("Rejecting Line " + floorRun);
			}
			//Valid Line, e.g. 8:1-3,9-5 
			else
			{
				String[] floorRunTokens = floorRun.split(":");

				//e.g. 8
				int startFloor = Integer.valueOf(floorRunTokens[0]).intValue();

				//Split the requests e.g. 1-3,9-5
				Queue<Request> requests = new LinkedList<>();
				String[] floorRequestTokens = floorRunTokens[1].split(",");
				for (String floorRequestToken : floorRequestTokens)
				{
					String[] floors = floorRequestToken.split("-");
					requests.add(new Request(Integer.valueOf(floors[0]).intValue(), Integer.valueOf(floors[1]).intValue()));
				}

				//Add 
				processedFloorRuns.add(new FloorRun(startFloor, requests));
			}
		}

		//Return
		return new InputData(processedFloorRuns, (Mode) validatedArguments.getRight());
	}

	/**
	 * Is Line Valid against a set of three rules. 
	 * 
	 * @param floorRun
	 * @return
	 */
	protected boolean isValidLine(String floorRun)
	{
		//If line is in correct format 
		if (!floorRun.matches("\\d{1,2}:(\\d{1,2}-\\d{1,2})(,\\d{1,2}-\\d{1,2}){0,}"))
		{
			return false;
		}

		//If floor numbers are valid i.e numbers between 1 and 12
		String[] floorTokens = floorRun.replace(":", ",").replace("-", ",").split(",");
		for (String floorToken : floorTokens)
		{
			Integer floor = Integer.valueOf(floorToken);
			if ((floor.intValue() < 1) || (floor.intValue() > 12))
			{
				return false;
			}
		}

		//If a start floor & end floor are the same.
		for (int i = 1; i < floorTokens.length; i += 2)
		{
			//Get floors
			Integer startFloor = Integer.valueOf(floorTokens[i]);
			Integer endFloor = Integer.valueOf(floorTokens[(i + 1)]);

			//Test equivalent
			if (startFloor == endFloor)
			{
				return false;
			}
		}

		//Return - all rules passed
		return true;
	}
}