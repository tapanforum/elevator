package com.gfx.elevator.service.algo;

import com.gfx.elevator.model.Direction;
import com.gfx.elevator.model.Request;
import com.gfx.elevator.model.Response;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Base Move Strategy class.
 * 
 * @author tmokha
 */
public abstract class BaseMoveStrategy implements MoveStrategy
{
	protected Deque<Integer> responseQueue;

	public BaseMoveStrategy()
	{
		this.responseQueue = new ArrayDeque<>();
	}

	/**
	 * Point of entry for a move strategy.
	 */
	public Response execute(int startFloor, Queue<Request> requestQueue)
	{
		this.responseQueue.add(Integer.valueOf(startFloor));

		processRequests(requestQueue);

		return createResponse();
	}

	protected abstract void processRequests(Queue<Request> paramQueue);

	/**
	 * Used to store all requests that are to be processed together. 
	 * 
	 * @param currentDirection
	 * @return
	 */
	protected SortedSet<Integer> createFloorVisitSet(Direction currentDirection)
	{
		SortedSet<Integer> floorVisitSet = null;

		if (currentDirection == Direction.UP)
			floorVisitSet = new TreeSet<>();
		else
		{
			floorVisitSet = new TreeSet<>(Collections.reverseOrder());
		}
		return floorVisitSet;
	}

	/**
	 * Updates the responseQueue with floors to visit. 
	 * 
	 * @param floorVisits
	 */
	protected void updateFloorVisits(SortedSet<Integer> floorVisits)
	{
		//If the elevator after the last request is on same floor as the first floor of the incoming request 
		//then there is no movement needed, i.e. the floor should not be add to responseQueue.
		if (((Integer) floorVisits.first()).equals(this.responseQueue.getLast()))
		{
			floorVisits.remove(floorVisits.first());
		}

		//Add rest of the floors
		this.responseQueue.addAll(floorVisits);
	}

	/**
	 * Calculates the distance in floors traveled by the elevator from the path in responseQueue. 
	 * 
	 * @return
	 */
	protected int calculateFloorCount()
	{
		Integer[] floors = (Integer[]) this.responseQueue.toArray(new Integer[0]);

		int count = 0;
		for (int i = 0; i < floors.length; i++)
		{
			int thisFloor = floors[i].intValue();

			int nextfloor = 0;
			if (i + 1 >= floors.length)
			{
				break;
			}
			nextfloor = floors[(i + 1)].intValue();

			count += Math.abs(thisFloor - nextfloor);
		}

		return count;
	}

	/**
	 * Creates response.
	 * 
	 * @return
	 */
	protected Response createResponse()
	{
		return new Response(this.responseQueue, calculateFloorCount());
	}
}