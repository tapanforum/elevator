package com.gfx.elevator.service.algo;

import com.gfx.elevator.model.Direction;
import com.gfx.elevator.model.Request;
import java.util.Iterator;
import java.util.Queue;
import java.util.SortedSet;

/**
 * A move Strategy implementation done Optimally i.e processes each request taking into consideration consecutive requests.
 * 
 * @author tmokha
 */
public class OptimizedMoveStrategy extends BaseMoveStrategy
{
	/**
	 * Core Move Logic
	 */
	public void processRequests(Queue<Request> requestQueue)
	{
		//Current movement direction is determined from first request
		Direction currentDirection = ((Request) requestQueue.peek()).getDirection();

		//temp variable to store to be processed requests
		SortedSet<Integer> floorVisits = createFloorVisitSet(currentDirection);

		//Foreach request
		Iterator<Request> requestIterator = requestQueue.iterator();
		while (requestIterator.hasNext())
		{
			Request request = (Request) requestIterator.next();

			//Is this request's direction same as current direction, if not process all accumulated request    
			if (request.getDirection() != currentDirection)
			{
				break;
			}
			
			//Add floors from request
			floorVisits.add(Integer.valueOf(request.getStartFloor()));
			floorVisits.add(Integer.valueOf(request.getEndFloor()));

			//Remove current request from queue
			requestIterator.remove();
		}

		//Update floor visits - process all accumulated request    
		updateFloorVisits(floorVisits);

		//If more requests exist, process them
		if (!requestQueue.isEmpty())
		{
			processRequests(requestQueue);
		}
	}
}