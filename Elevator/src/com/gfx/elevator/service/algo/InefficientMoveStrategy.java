package com.gfx.elevator.service.algo;

import com.gfx.elevator.model.Request;
import java.util.Queue;
import java.util.SortedSet;

/**
 * A move Strategy implementation done Inefficiently i.e processes each request independently without taking into consideration any consecutive requests. 
 * 
 * @author tmokha
  */
public class InefficientMoveStrategy extends BaseMoveStrategy
{
	/**
	 * Core Move Logic
	 */
	public void processRequests(Queue<Request> requestQueue)
	{
		//Foreach request
		for (Request request : requestQueue)
		{
			//temp variable to store to be processed requests
			SortedSet<Integer> floorVisitSet = createFloorVisitSet(request.getDirection());

			//Add floors from request
			floorVisitSet.add(Integer.valueOf(request.getStartFloor()));
			floorVisitSet.add(Integer.valueOf(request.getEndFloor()));

			//Update floor visits
			updateFloorVisits(floorVisitSet);
		}
	}
}