package com.gfx.elevator.service.algo;

import java.util.Queue;

import com.gfx.elevator.model.Mode;
import com.gfx.elevator.model.Request;
import com.gfx.elevator.model.Response;

/**
 * Base Strategy interface. Uses the Strategy design pattern.
 * 
 * @author tmokha
 */
public abstract interface MoveStrategy
{
	public abstract Response execute(int paramInt, Queue<Request> paramQueue);

	public static MoveStrategy instance(Mode mode)
	{
		switch (mode)
		{
			case ModeA:
				return new InefficientMoveStrategy();
			case ModeB:
				return new OptimizedMoveStrategy();
			default:
				throw new RuntimeException("Unhandled Mode" + mode);
		}
	}
}