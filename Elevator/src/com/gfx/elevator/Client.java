package com.gfx.elevator;

import com.gfx.elevator.model.FloorRun;
import com.gfx.elevator.model.InputData;
import com.gfx.elevator.model.Response;
import com.gfx.elevator.service.ElevatorService;
import com.gfx.elevator.service.ValidationService;
import com.gfx.elevator.util.Log;

/**
 * Point of Entry Class of Application
 * 
 * @author tmokha
 */
public class Client
{
	/**
	 * Point of Entry of Application
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Validate Input
		InputData inputData = validateInput(args);

		// Process Input
		processInput(inputData);
	}

	/**
	 * Validate Input
	 * 
	 * @param args
	 * @return
	 */
	private static InputData validateInput(String[] args)
	{
		//Init
		ValidationService validationService = new ValidationService();

		//Validate
		InputData inputData = validationService.validate(args);

		//Return
		return inputData;
	}

	/**
	 * Process Input
	 * 
	 * @param inputData
	 */
	private static void processInput(InputData inputData)
	{
		//Foreach FloorRun
		for (FloorRun floorRun : inputData.getFloorRuns())
		{
			//Init
			ElevatorService elevatorService = new ElevatorService(floorRun, inputData.getMode());
			
			//Process FlorrRun
			Response response = elevatorService.execute();

			//Log Response
			Log.log(response.getDisplayString());
		}
	}
}