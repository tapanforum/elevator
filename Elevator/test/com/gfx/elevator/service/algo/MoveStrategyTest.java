package com.gfx.elevator.service.algo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayDeque;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import com.gfx.elevator.model.Request;
import com.gfx.elevator.model.Response;

public class MoveStrategyTest
{
  private OptimizedMoveStrategy optimizedMoveStrategy;
  private InefficientMoveStrategy inefficientMoveStrategy;

  @Before
  public void setUp()
  {
    inefficientMoveStrategy = new InefficientMoveStrategy();
    optimizedMoveStrategy = new OptimizedMoveStrategy();
  }

  @Test
  public void ensureModeAWorksForSingleCall()
  {
    Queue<Request> requestQueue = getSingleCallQueue();
    Response response = inefficientMoveStrategy.execute(10, requestQueue);

    assertThat(response.getQueue(), contains( 10, 8, 1 ));
    assertThat(response.getCount(), is(equalTo(9)));
  }

  @Test
  public void ensureModeBWorksForSingleCall()
  {
    Queue<Request> requestQueue = getSingleCallQueue();
    Response response = optimizedMoveStrategy.execute(10, requestQueue);

    assertThat(response.getQueue(), contains( 10, 8, 1 ));
    assertThat(response.getCount(), is(equalTo(9)));
  }

  @Test
  public void ensureModeAWorksForFirstCallFromSameFloor()
  {
    Queue<Request> requestQueue = getSingleCallQueue();
    Response response = inefficientMoveStrategy.execute(8, requestQueue);

    assertThat(response.getQueue(), contains( 8, 1 ));
    assertThat(response.getCount(), is(equalTo(7)));
  }

  @Test
  public void ensureModeBWorksForFirstCallFromSameFloor()
  {
    Queue<Request> requestQueue = getSingleCallQueue();
    Response response = optimizedMoveStrategy.execute(8, requestQueue);

    assertThat(response.getQueue(), contains( 8, 1 ));
    assertThat(response.getCount(), is(equalTo(7)));
  }

  @Test
  public void ensureModeAWorksForConsecutiveCallFromSameFloor()
  {
	    Queue<Request> requestQueue = getConsecutiveCallFromSameFloorQueue();
	    Response response = inefficientMoveStrategy.execute(3, requestQueue);

	    assertThat(response.getQueue(), contains( 3, 11, 6, 5, 6 ));
	    assertThat(response.getCount(), is(equalTo(15)));
  }

  @Test
  public void ensureModeBWorksForConsecutiveCallFromSameFloor()
  {
	    Queue<Request> requestQueue = getConsecutiveCallFromSameFloorQueue();
	    Response response = optimizedMoveStrategy.execute(3, requestQueue);

	    assertThat(response.getQueue(), contains( 3, 11, 6, 5, 6 ));
	    assertThat(response.getCount(), is(equalTo(15)));
  }

  @Test
  public void ensureModeBWorksForThreeSameDirectionFollowedByOppositeDirectionCall()
  {
    Queue<Request> requestQueue = getThreeSameDirectionCallQueue();
    Response response = optimizedMoveStrategy.execute(7, requestQueue);

    assertThat(response.getQueue(), contains( 7, 11, 10, 8, 6, 5, 6, 11 ));
    assertThat(response.getCount(), is(equalTo(16)));
  }

  private Queue<Request> getSingleCallQueue()
  {
    Queue<Request> queue = new ArrayDeque<>();
    queue.add(new Request(8, 1));
    return queue;
  }

  private Queue<Request> getConsecutiveCallFromSameFloorQueue()
  {
    Queue<Request> queue = new ArrayDeque<>();
    queue.add(new Request(11, 6));
    queue.add(new Request(6, 5));
    queue.add(new Request(5, 6));
    return queue;
  }

  private Queue<Request> getThreeSameDirectionCallQueue()
  {
    Queue<Request> queue = new ArrayDeque<>();
    queue.add(new Request(11, 6));
    queue.add(new Request(10, 5));
    queue.add(new Request(8, 6));
    queue.add(new Request(6, 11));
    return queue;
  }
}