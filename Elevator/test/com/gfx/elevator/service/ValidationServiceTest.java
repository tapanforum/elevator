package com.gfx.elevator.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.contains;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;

import com.gfx.elevator.Client;
import com.gfx.elevator.model.FloorRun;
import com.gfx.elevator.model.InputData;
import com.gfx.elevator.model.Mode;
import com.gfx.elevator.model.Request;
import com.google.common.collect.Lists;

public class ValidationServiceTest
{
	private ValidationService validationService;

	@Before
	public void setUp()
	{
		validationService = new ValidationService();
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidNumberOfArguments()
	{
		Client.main(new String[0]);
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidModeName()
	{
		Client.main(new String[]
		{ "fileName", "modeC" });
	}

	@Test(expected = IllegalArgumentException.class)
	public void fileDoesNotExistTest()
	{
		Client.main(new String[]
		{ "fileName", "modeB" });
	}

	@Test
	public void positiveValidArgumentTest()
	{
		Client.main(new String[]{ "test\\com\\gfx\\elevator\\service\\testInputFile", "modeB" });
	}

	@Test
	public void invalidFloorRunFormatEmptyString()
	{
		boolean validLine = validationService.isValidLine("");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void invalidFloorRunFormatStartFloorOnly()
	{
		boolean validLine = validationService.isValidLine("8:");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void invalidFloorRunFormatDanglingComma()
	{
		boolean validLine = validationService.isValidLine("8:1-2,");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void invalidFloorRunFormatAlpha()
	{
		boolean validLine = validationService.isValidLine("8:a-2");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void invalidFloorRunFloorNumberGreaterThan12()
	{
		boolean validLine = validationService.isValidLine("8:13-2");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void invalidFloorRunFloorNumbersAreSame()
	{
		boolean validLine = validationService.isValidLine("8:1-1");
		assertThat(validLine, equalTo(false));
	}

	@Test
	public void validFloorRun()
	{
		boolean validLine = validationService.isValidLine("3:7-9,3-7,5-8,7-11,11-1");
		assertThat(validLine, equalTo(true));
	}

	@Test
	public void validFloorRunForOddNumberofRequests()
	{
		boolean validLine = validationService.isValidLine("3:7-9");
		assertThat(validLine, equalTo(true));
	}

	@Test
	public void validFloorRunForEvenNumberofRequests()
	{
		boolean validLine = validationService.isValidLine("3:7-9,3-7");
		assertThat(validLine, equalTo(true));
	}

	@Test
	public void validateDataMethod()
	{
		InputData validateData = validationService.validateData(ImmutablePair.of(Lists.newArrayList(new String[]{ "3:7-9,3-7,11-1" }), Mode.ModeA));

		assertThat(Integer.valueOf(((FloorRun) validateData.getFloorRuns().get(0)).getStartFloor()), equalTo(3));

		assertThat(((FloorRun) validateData.getFloorRuns().get(0)).getQueue(), contains(new Request[]{ new Request(7, 9), new Request(3, 7), new Request(11, 1) }));
	}
}