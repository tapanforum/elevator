package com.gfx.elevator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.gfx.elevator.service.ValidationServiceTest;
import com.gfx.elevator.service.algo.MoveStrategyTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ValidationServiceTest.class, MoveStrategyTest.class})
public class AllTests
{
}